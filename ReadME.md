# Projet Économétrie Avancée avec R 

Identifier une typologie de communes qui se ressemblent

# Analyse à l'aide des modèles ACP, HC et Logit

L’objectif est de dégager une typologie permettant des regroupements de communes selon 

la plus ou moins importante présence de médecins, d’équipements d’ascenseurs dans les habitations, 

d’accès aux soins pour les personnes âgées, questions sur la qualité de vie et la santé des personnes 

âgées selon les tranches d’âges… Certaines données sont en médiane ou absentes, il est donc nécessaire 

d’utiliser dans ce cas des données de la base département à utiliser pour les communes du département.

L’idée est de faire des analyses en composante principales (ACP) et de la classification hiérarchique 

pour dégager des typologies de communes. Les données sont en libre accès sur le site open data de la 

Caisse des Dépôts. Le lien pour télécharger la base communale est ici : 

https://opendata.caissedesdepots.fr/explore/dataset/60-et-plus_indicateurs-au-niveau-de-la-commune/export/

Le lien vers la documentation des données est ici : 

https://opendata.caissedesdepots.fr/explore/dataset/60-et-plus_indicateurs-au-niveau-de-la-commune/information/

Le lien vers la totalité des bases (communale et départementale) est ici : 

https://opendata.caissedesdepots.fr/explore/?q=vieillissement&sort=modified

Quand on sélectionne une base donnée, il faut aller sur l’onglet « information » pour avoir le dictionnaire 

des données et « export » pour télécharger les données.

